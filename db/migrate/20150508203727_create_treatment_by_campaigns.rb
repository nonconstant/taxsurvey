class CreateTreatmentByCampaigns < ActiveRecord::Migration
  def change
    create_table :treatment_by_campaigns do |t|
      t.string :title
      t.integer :index

      t.timestamps null: false
    end
  end
end

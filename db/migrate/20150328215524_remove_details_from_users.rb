class RemoveDetailsFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :privatefund, :boolean
    remove_column :users, :sex, :boolean
  end
end

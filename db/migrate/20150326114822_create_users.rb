class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.integer :age
      t.boolean :sex
      t.integer :educ
      t.string :uni
      t.string :occup
      t.string :residence
      t.float :aftertaxwage
      t.integer :elec_presid
      t.float :taxest
      t.integer :disapprove
      t.integer :donate
      t.text :media
      t.integer :statefin
      t.datetime :created

      t.timestamps null: false
    end
  end
end

# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150522195901) do

  create_table "budgets", force: :cascade do |t|
    t.integer  "code"
    t.string   "name"
    t.float    "total"
    t.float    "percap"
    t.text     "spending"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.text     "s_soc"
    t.text     "s_educ"
    t.text     "s_health"
    t.text     "s_econ"
    t.text     "s_util"
    t.text     "s_state"
    t.text     "s_secur"
    t.text     "s_culture"
    t.text     "s_defence"
    t.text     "s_debt"
    t.text     "s_sport"
    t.text     "s_media"
    t.text     "s_eco"
    t.text     "s_transfers"
    t.text     "i_subs"
  end

  create_table "treatment_by_campaigns", force: :cascade do |t|
    t.string   "title"
    t.integer  "index"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "treatmentby_regions", force: :cascade do |t|
    t.text     "byeduc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "treatments", force: :cascade do |t|
    t.text     "treat"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.integer  "age"
    t.integer  "educ"
    t.string   "uni"
    t.string   "occup"
    t.string   "residence"
    t.float    "aftertaxwage"
    t.integer  "elec_presid"
    t.float    "taxest"
    t.integer  "disapprove"
    t.integer  "donate"
    t.text     "media"
    t.integer  "statefin"
    t.datetime "created"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.float    "treattax"
    t.float    "treatapprove"
    t.float    "treatdonate"
    t.integer  "treatment"
    t.float    "paid_ndfl"
    t.float    "paid_other"
    t.float    "paid_pfr"
    t.float    "paid_fss"
    t.float    "paid_foms"
    t.integer  "privatefund"
    t.integer  "sex"
    t.string   "media_other"
    t.string   "source"
    t.integer  "consent"
    t.float    "wagecat"
    t.float    "wagerange"
    t.string   "uniother"
    t.string   "ref"
    t.string   "campaign"
    t.integer  "petit_spending"
    t.integer  "petit_account"
    t.string   "uni_other"
    t.float    "taxcertainty"
    t.integer  "media1"
    t.integer  "media2"
    t.integer  "media3"
    t.integer  "media4"
    t.integer  "media5"
    t.integer  "media6"
    t.integer  "media7"
    t.integer  "media8"
    t.integer  "media9"
    t.integer  "media10"
    t.integer  "qasked"
    t.string   "useragent"
    t.string   "httpref"
    t.string   "ipaddress"
  end

end

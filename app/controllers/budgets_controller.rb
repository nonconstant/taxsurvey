# encoding: UTF-8

class BudgetsController < ApplicationController
  before_action :set_budget, only: [:show, :edit, :update, :destroy, :share]
  http_basic_authenticate_with name: "misha", password: "vredina", only: :index
  # GET /budgets/1
  # GET /budget/1.json
  def show
  end

  # GET /budgets/new
  def new
    @budget = Budget.new()
  end

  # GET /budgets/1/edit
  def edit
  end
  
  def convert_to_utf8_encoding(original_file)  
    original_string = original_file.read
    final_string = original_string.encode('utf-8', 'binary', invalid: :replace, undef: :replace, replace: '')
    puts final_string
    final_file = Tempfile.new('import') #No need to save a real File
    final_file.write(final_string)
    final_file.close #Don't forget me
    final_file
  end 
  
  
  def import
    orig = params[:file].read
    clean = orig.encode('utf-8', 'binary', invalid: :replace, undef: :replace, replace: '')
    clean = clean.gsub("\n", "")
    params[:file].close
    cfile = Tempfile.new('import') #No need to save a real File
    cfile.write(clean)
    CSV.foreach(cfile.path, headers: true, col_sep: ';', converters: :numeric) do |row|
      puts row[0]
      title = "-"
      case row[0]
      when 77 
        title = "Москва"
      when 78 
        title = "Санкт-Петербург"
      when 1 
        title = "Республика Адыгея"
      when 4 
        title = "Республика Алтай"
      when 2 
        title = "Республика Башкортостан"
      when 3 
        title = "Республика Бурятия"
      when 5 
        title = "Республика Дагестан"
      when 6 
        title = "Республика Ингушетия"
      when 7 
        title = "Кабардино-Балкарская республика"
      when 8 
        title = "Республика Калмыкия"
      when 9 
        title = "Карачаево-Черкесская республика"
      when 10 
        title = "Республика Карелия"
      when 11 
        title = "Республика Коми"
      when 91 
        title = "Республика Крым"
      when 12 
        title = "Республика Марий Эл"
      when 13 
        title = "Республика Мордовия"
      when 14 
        title = "Республика Саха (Якутия)"
      when 15 
        title = "Республика Северная Осетия — Алания"
      when 16 
        title = "Республика Татарстан"
      when 17 
        title = "Республика Тыва"
      when 18 
        title = "Удмуртская республика"
      when 19 
        title = "Республика Хакасия"
      when 20 
        title = "Чеченская республика"
      when 21 
        title = "Чувашская республика"
      when 22 
        title = "Алтайский край"
      when 75 
        title = "Забайкальский край"
      when 41 
        title = "Камчатский край"
      when 23 
        title = "Краснодарский край"
      when 24 
        title = "Красноярский край"
      when 59 
        title = "Пермский край"
      when 25 
        title = "Приморский край"
      when 26 
        title = "Ставропольский край"
      when 27 
        title = "Хабаровский край"
      when 28 
        title = "Амурская область"
      when 29 
        title = "Архангельская область"
      when 30 
        title = "Астраханская область"
      when 31 
        title = "Белгородская область"
      when 32 
        title = "Брянская область"
      when 33 
        title = "Владимирская область"
      when 34 
        title = "Волгоградская область"
      when 35 
        title = "Вологодская область"
      when 36 
        title = "Воронежская область"
      when 37 
        title = "Ивановская область"
      when 38 
        title = "Иркутская область"
      when 39 
        title = "Калининградская область"
      when 40 
        title = "Калужская область"
      when 42 
        title = "Кемеровская область"
      when 43 
        title = "Кировская область"
      when 44 
        title = "Костромская область"
      when 45 
        title = "Курганская область"
      when 46 
        title = "Курская область"
      when 47 
        title = "Ленинградская область"
      when 48 
        title = "Липецкая область"
      when 49 
        title = "Магаданская область"
      when 50 
        title = "Московская область"
      when 51 
        title = "Мурманская область"
      when 52 
        title = "Нижегородская область"
      when 53 
        title = "Новгородская область"
      when 54 
        title = "Новосибирская область"
      when 55 
        title = "Омская область"
      when 56 
        title = "Оренбургская область"
      when 57 
        title = "Орловская область"
      when 58 
        title = "Пензенская область"
      when 60 
        title = "Псковская область"
      when 61 
        title = "Ростовская область"
      when 62 
        title = "Рязанская область"
      when 63 
        title = "Самарская область"
      when 64 
        title = "Саратовская область"
      when 65 
        title = "Сахалинская область"
      when 66 
        title = "Свердловская область"
      when 67 
        title = "Смоленская область"
      when 68 
        title = "Тамбовская область"
      when 69 
        title = "Тверская область"
      when 70 
        title = "Томская область"
      when 71 
        title = "Тульская область"
      when 72 
        title = "Тюменская область"
      when 73 
        title = "Ульяновская область"
      when 74 
        title = "Челябинская область"
      when 76 
        title = "Ярославская область"
      when 92 
        title = "Севастополь"
      when 79 
        title = "Еврейская автономная область"
      when 83 
        title = "Ненецкий автономный округ"
      when 86 
        title = "Ханты-Мансийский автономный округ - Югра"
      when 87 
        title = "Чукотский автономный округ"
      when 89 
        title = "Ямало-Ненецкий автономный округ"
      end
      
      (5..18).each do |i|
        numstr = row[i].tr("[]","").split(",")
        puts "Numbers split"
        puts numstr
        numbers = []
        numstr.each do |numk|
          numbers << numk.to_f
        end
        row[i] = numbers
      end
    
      
      @budget = Budget.create(code: row[0], name: title, total: row[2], percap: row[3], 
      s_soc: row[5], s_educ: row[6],	s_health: row[7],	s_econ: row[8],	s_util: row[9],
      s_state: row[10],	s_secur: row[11],	s_culture: row[12],	s_defence: row[13],	s_debt: row[14],
      s_sport: row[15],	s_media: row[16],	s_eco: row[17],	s_transfers: row[18],	i_subs: row[19])
      @budget.save
    end
    
    cfile.close
    redirect_to :index
  end
  
  
  
  
    # PATCH/PUT /budgets/1
  # PATCH/PUT /budgets/1.json
  def update
    respond_to do |format|
      if @budget.update(budget_params)
        format.html { render :show }
        format.json { render :show, status: :ok, location: @budget}
      else
        format.html { render :edit }
        format.json { render json: @budget.errors, status: :unprocessable_entity }
      end
    end
  end


  # DELETE /budgets/1
  # DELETE /budgets/1.json
  def destroy
    @budget.destroy
    respond_to do |format|
      format.html { redirect_to budgets_url, notice: 'budget was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def create
    
  end
  
  def after_upload
    
    @budgets.each do |bud|
      bud.attributes.each_pair do |t_attr_name, shares|
        if t_attr_name.start_with?("s_")
          numbers = shares.split(",").to_s
          numbers = numbers.tr("[]","")
          
          bud.update_column(t_attr_name, numbers )
        end
      end
    end
  end
  # GET /budgets
  # GET /budgets.json
  def index
    @budgets = Budget.all
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_budget
      @budget = Budget.find(params[:id])
    end
end
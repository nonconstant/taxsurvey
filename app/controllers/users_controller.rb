class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy, :share]
  http_basic_authenticate_with name: "misha", password: "lapochka", only: [:index, :show, :edit, :destroy]


  def index
    @users = User.order(:id)
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    
    if cookies[:user_created_id] != nil
      puts "user_created_id" + cookies[:user_created_id].to_s
      if (User.exists?(cookies[:user_created_id])) then
        @user = User.find(cookies[:user_created_id])
        render :share
      else
        @user = User.new(consent: cookies[:user_consent], aftertaxwage: cookies[:user_aftertaxwage], wagerange: cookies[:user_wagerange], age: cookies[:user_age], sex: cookies[:user_sex], residence: cookies[:user_residence], educ: cookies[:user_educ], uni: cookies[:user_uni], uniother: cookies[:user_uniother], statefin: cookies[:user_statefin], residence: cookies[:user_residence], occup: cookies[:user_occup], elec_presid: cookies[:user_elec_presid], taxest: cookies[:user_taxest], taxcertainty: cookies[:user_taxcertainty], media: cookies[:user_media], media1: cookies[:user_media1], media2: cookies[:user_media2], media3: cookies[:user_media3], media4: cookies[:user_media4], media5: cookies[:user_media5], media6: cookies[:user_media6], media7: cookies[:user_media7], media8: cookies[:user_media8], media9: cookies[:user_media9])
      end
    else
      @user = User.new(consent: cookies[:user_consent], aftertaxwage: cookies[:user_aftertaxwage], wagerange: cookies[:user_wagerange], age: cookies[:user_age], sex: cookies[:user_sex], residence: cookies[:user_residence], educ: cookies[:user_educ], uni: cookies[:user_uni], uniother: cookies[:user_uniother], statefin: cookies[:user_statefin], residence: cookies[:user_residence], occup: cookies[:user_occup], elec_presid: cookies[:user_elec_presid], taxest: cookies[:user_taxest], taxcertainty: cookies[:user_taxcertainty], media: cookies[:user_media], media1: cookies[:user_media1], media2: cookies[:user_media2], media3: cookies[:user_media3], media4: cookies[:user_media4], media5: cookies[:user_media5], media6: cookies[:user_media6], media7: cookies[:user_media7], media8: cookies[:user_media8], media9: cookies[:user_media9])
      puts "New user"
    end
    
  end

  # GET /users/1/edit
  def edit
  end
  
  def showtaxes
    @user = User.find(params[:id])
  end
  
  
  def anotheruser
    @user = User.new(aftertaxwage:0, wagerange:0, ref:cookies[:user_created_id])
    cookies.permanent[:user_ref] = cookies[:user_created_id]
    cookies.delete :user_created_id
    render :new
  end
  
  def showspending
    @budgets = Budget.all
    @user = User.find(params[:id])
    @titles = Budget.find_by code: '0'
    @federal = Budget.find_by code: '-1'
    @pfr = Budget.find_by code: '-2'
    @fss = Budget.find_by code: '-3'
    @foms = Budget.find_by code: '-4'
    
    @region = Budget.find_by code: @user.residence
    if @region == nil
      @region = Budget.find_by code: '77'
    end
    
  end
  
  def askquestions
    @user = User.find(params[:id])
  end
  
  def share
    @user = User.find(params[:id])
  end
  def ndfl
    
    @user = User.find(params[:id])
    
  end
  
  def root
    if params[:campaign] != nil
      cookies.permanent[:user_campaign] = params[:campaign] 
      puts "Cookies << campaign = " + params[:campaign].to_s
    end
    if params[:source] != nil
      cookies.permanent[:user_source] = params[:source] 
      puts "Cookies << source = " + params[:source].to_s
    end
    cookies.permanent[:user_httpref] = request.env['HTTP_REFERER']
    puts "Cookies << httpref = " + request.env['HTTP_REFERER'].to_s
    cookies.permanent[:user_ipaddress] = request.remote_ip
    puts "Cookies << ipaddress = " + request.remote_ip.to_s
    redirect_to action: 'new'
  end

  # POST /users
  # POST /users.json
  def create
    
    cookies.permanent[:user_consent] = user_params[:consent] if user_params[:consent] != nil
    cookies.permanent[:user_aftertaxwage] = user_params[:aftertaxwage] if user_params[:aftertaxwage] != nil
    cookies.permanent[:user_wagerange] = user_params[:wagerange] if user_params[:wagerange] != nil
    cookies.permanent[:user_age] = user_params[:age] if user_params[:age] != nil
    cookies.permanent[:user_sex] = user_params[:sex] if user_params[:sex] != nil
    cookies.permanent[:user_educ] = user_params[:educ] if user_params[:educ] != nil
    cookies.permanent[:user_uni] = user_params[:uni] if user_params[:uni] != nil
    cookies.permanent[:user_uniother] = user_params[:uniother] if user_params[:uniother] != nil
    cookies.permanent[:user_occup] = user_params[:occup] if user_params[:occup] != nil
    cookies.permanent[:user_residence] = user_params[:residence] if user_params[:residence] != nil
    cookies.permanent[:user_elec_presid] = user_params[:elec_presid] if user_params[:elec_presid] != nil
    cookies.permanent[:user_taxest] = user_params[:taxest] if user_params[:taxest] != nil
    cookies.permanent[:user_taxcertainty] = user_params[:taxcertainty] if user_params[:taxcertainty] != nil
    cookies.permanent[:user_media] = user_params[:media] if user_params[:media] != nil
    cookies.permanent[:user_media1] = user_params[:media1] if user_params[:media1] != nil
    cookies.permanent[:user_media2] = user_params[:media2] if user_params[:media2] != nil
    cookies.permanent[:user_media3] = user_params[:media3] if user_params[:media3] != nil
    cookies.permanent[:user_media4] = user_params[:media4] if user_params[:media4] != nil
    cookies.permanent[:user_media5] = user_params[:media5] if user_params[:media5] != nil
    cookies.permanent[:user_media6] = user_params[:media6] if user_params[:media6] != nil
    cookies.permanent[:user_media7] = user_params[:media7] if user_params[:media7] != nil
    cookies.permanent[:user_media8] = user_params[:media8] if user_params[:media8] != nil
    cookies.permanent[:user_media9] = user_params[:media9] if user_params[:media9] != nil
    cookies.permanent[:user_media10] = user_params[:media10] if user_params[:media10] != nil
    cookies.permanent[:user_statefin] = user_params[:statefin] if user_params[:statefin] != nil
    cookies.permanent[:user_media_other] = user_params[:media_other] if user_params[:media_other] != nil
    
    @user = User.new(user_params)        
    respond_to do |format|
      if @user.save
        puts "saved user"
        if @user.wagerange.nil?
          @user.update_column(:wagerange, 0)
          puts "set wagerange to 0"
        end  
        cookies.permanent[:user_created_id] = @user.id
        pretax =  @user.aftertaxwage.to_i/0.87
        puts pretax
        limit14 = 624000 
        @user.update_column(:paid_ndfl, pretax * 12 * 0.13)
        
        @user.update_column(:paid_pfr, [pretax*12, limit14].min * 0.22 + [pretax*12 - limit14, 0].max *  0.1)
        
        @user.update_column(:paid_fss, [pretax*12, limit14].min*0.029)
        
        @user.update_column(:paid_foms, [pretax*12, limit14].min*0.051)
        
        @user.update_column(:paid_other, pretax*12*0.241220)
        
        @user.update_column(:useragent, request.env['HTTP_USER_AGENT'])
        
        @user.update_column(:httpref, cookies[:user_httpref]) if !cookies[:user_httpref].nil?
        
        @user.update_column(:ipaddress, cookies[:user_ipaddress]) if !cookies[:user_ipaddress].nil?
        
        @user.update_column(:ref, cookies[:user_ref]) if !cookies[:user_ref].nil?
        
        @user.update_column(:source, cookies[:user_source]) if !cookies[:user_source].nil?
        
        @user.update_column(:campaign, cookies[:user_campaign]) if !cookies[:user_campaign].nil?
        
        
#РАНДОМИЗАЦИЯ
        
        puts 'calc treatment'
        TreatmentByCampaign.all.each do |t|
          puts "campaign = " + t.title.to_s + " treatindex = " + t.index.to_s
        end
        @tc = TreatmentByCampaign.find_by title:@user.campaign
        if (@tc.nil? )
          #Название кампании не найдено
          @tc = TreatmentByCampaign.new(title:@user.campaign, index:-1)
        end
        @treat = @tc.index
        if (@treat == -1)
          @user.update_column(:treatment, rand(2))
          @tc.index = @user.treatment
          @tc.save
          # 1 - показываем вопросы до showtaxes, 0 - после showtaxes          
        else
          @user.update_column(:treatment, 1 - @treat)
          @tc.index = -1
          @tc.save
        end
          
=begin        
        @treatreg = TreatmentbyRegion.find(@user.residence)
        @treat = @treatreg.byeduc[@user.educ].to_i
        puts 'treat vector old ' + @treatreg.byeduc.to_s
        puts 'educ index ' + @user.educ.to_s
        if (@treat == -1)
          @user.update_column(:treatment, rand(2))
          @treatreg.byeduc[@user.educ] = @user.treatment
          @treatreg.save
          
# 1 - показываем вопросы до showtaxes, 0 - после showtaxes          
        else
          @user.update_column(:treatment, 1 - @treat)
          @treatreg.byeduc[@user.educ] = -1
          @treatreg.save
        end
=end
        
        puts 'treat vector new ' + @treat.to_s
        format.html { 
            redirect_to action: "ndfl", id: @user.id and return }
        format.json { redirect_to action: "ndfl", id: @user.id and return }
      else
#Сюда попадают с недозаполненной формой после нажатия "Отправить"
        puts "Couldn't save user"
        @user.errors.each do |e|
          puts e.to_s
        end
        format.html { 
          puts "rendering new"
          render action: 'new', location: 'user_new'
        }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def tforward (treatment, stage, usrid)
    case stage
    when "askquestions"
      if treatment == 1
        redirect_to action: "showtaxes", id: usrid
      elsif @user.treatment == 0
        redirect_to action: "showspending", id: usrid
      else
        redirect_to action: "share", id: usrid
      end
    end
  end
  

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  
  
  def update
    puts "--------------UPDATE"
    respond_to do |format|
      if !params[:user].nil?
        if (!@user.treattax.nil?)
          if @user.treattax>0
            tforward(@user.treatment, "askquestions", @user.id) and return
          
          end
        end
        if @user.update(user_params)
          format.html { 
            if (user_params[:treattax].nil? || user_params[:treatapprove].nil? || user_params[:petit_spending].nil? || user_params[:petit_account].nil?)
              @user.errors << "Пожалуйста, заполните все поля"
              render :askquestions and return
            end
            tforward(@user.treatment, "askquestions", @user.id) and return
          }
          format.json { render :share, status: :ok, location: @user}
        else
          format.html { render :askquestions }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      else
        @user.errors[:treattax] << "Пожалуйста, заполните все поля"
        format.html { render :askquestions }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end


  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user ).permit(:age, :wagerange, :sex, :educ, :uni, :uniother, :occup, :residence, :aftertaxwage, :elec_presid, :taxest, :treatment, :media, :statefin, :created, :treattax, :treatapprove, :treatdonate, :created_at, :media_other, :source, :consent, :ref, :campaign, :petit_spending, :petit_account, :updated_at, :taxcertainty, :media1, :media2, :media3, :media4, :media5, :media6, :media7, :media8, :media9, :media10)
    end
    
end

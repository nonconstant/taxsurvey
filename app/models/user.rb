class User < ActiveRecord::Base

attr_accessor :consent

  validates :consent, acceptance: {message: "Для продолжения необходимо согласие" }, on: :create
  validates :aftertaxwage, presence: {message: "Укажите значение" }, on: :create
  validates :taxest, presence: {message: "Укажите значение" }, on: :create
  validates :taxcertainty, presence: {message: "Укажите значение" }, on: :create
  validates :educ, presence: {message: "Выберите из списка" }, on: :create
  validates :educ, numericality: true, on: :create
  validates :residence, presence: {message: "Выберите из списка" }, on: :create
  validates :residence, numericality: true, on: :create
  validates :occup, presence: {message: "Выберите из списка" }, on: :create
  validates :occup, numericality: true, on: :create
  validates :elec_presid, presence: {message: "Выберите из списка" }, on: :create
  validates :elec_presid, numericality: true, on: :create
  validates :age, presence: {message: "Укажите значение"}, on: :create
  validates :age, inclusion: {in: 14..99, message: "Возраст не может быть меньше 14 лет" }, on: :create
  validates :sex, presence: {message: "Выберите значение"}, on: :create
  
  validates :treattax, presence: {message: "Укажите значение" }, on: :update
  validates :treatapprove, presence: {message: "Укажите значение" }, on: :update
  validates :petit_spending, presence: {message: "Укажите значение" }, on: :update
  validates :petit_account, presence: {message: "Укажите значение" }, on: :update


end

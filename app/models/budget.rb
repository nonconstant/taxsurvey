class Budget < ActiveRecord::Base
  serialize :s_soc, Array
  serialize :s_health, Array
  serialize :s_educ, Array
  serialize :s_util, Array
  serialize :s_econ, Array
  serialize :s_eco, Array
  serialize :s_secur, Array
  serialize :s_defence, Array
  serialize :s_culture, Array
  serialize :s_sport, Array
  serialize :s_media, Array
  serialize :s_transfers, Array
  serialize :s_state, Array
  serialize :s_debt, Array
end

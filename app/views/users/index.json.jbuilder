json.array!(@users) do |user|
  json.extract! user, :id, :age, :sex, :educ, :uni, :occup, :residence, :aftertaxwage, :elec_presid, :taxest, :disapprove, :donate, :media, :statefin, :created
  json.url user_url(user, format: :json)
end

Rails.application.routes.draw do

  resources :budgets do  
    collection { post :import }
    get 'createbudget' => 'budgets#create'
    get 'budgets' => 'budgets#index'
  end  


resources :users, only: [:update, :destroy]

get 'users', to: 'users#new'
get 'users/index', to: 'users#index'

get 'users/new', to: 'users#new', as: 'new_user'
get 'users/:id/new', to: 'users#new'
get 'users/:id/edit', to: 'users#edit', as: 'edit_user'
get 'users/:id', to: 'users#share'
post 'users/:id', to: 'users#update'
patch 'users/:id', to: 'users#update'
patch 'users/:id/update', to: 'users#update'
patch 'users/:id/new', to: 'users#update'
put 'users/:id', to: 'users#update'
delete 'users/:id', to: 'users#destroy'
delete 'users/:id/new', to: 'users#destroy'
get 'users/:id/ndfl', to: 'users#ndfl', as: 'ndfl_user'
get 'users/:id/showtaxes', to: 'users#showtaxes', as: 'showtaxes_user'
get 'users/:id/showspending', to: 'users#showspending', as: 'showspending_user'
get 'users/:id/askquestions', to: 'users#askquestions', as: 'askquestions_user'
get 'users/:id/anotheruser', to: 'users#anotheruser', as: 'anotheruser_user'
get 'users/:id/share' => 'users#share', as: 'share_user'
get 'treatments' => 'treatmentby_regions#index'
post 'users', to: 'users#create', as: 'user_create'
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
root 'users#root'

  # Example of regular route:

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
